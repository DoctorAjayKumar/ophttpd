-module(oh_client).
-vsn("0.1.0").

-export_type([
    start_options/0
]).
-export([start/1]).
-export([start_link/1, init/2]).
-export([system_continue/3, system_terminate/4,
         system_get_state/1, system_replace_state/2]).


%%% Type and Record Definitions

% options for starting
-type start_options() :: #{socket := gen_tcp:socket()}.

% Note the "socket" here in the CLIENT state is a CONNECTION socket,
% not a LISTEN socket
-record(s, {socket       = none           :: none | gen_tcp:socket(),
            parser_state = oh_parse:new() :: oh_parse:state()}).

-type state() :: #s{}.

-include("$zx_include/zx_logger.hrl").
-include("oh.hrl").

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% API: EXPORTED FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% api: startup

-spec start(StartOptions) -> Result
    when StartOptions :: start_options(),
         Result       :: {ok, pid()}
                       | {error, Reason},
         Reason       :: {already_started, pid()}
                       | {shutdown, term()}
                       | term().
% @private
% How the oh_client_man or a prior oh_client kicks things off.
% This is called in the context of oh_client_man or the prior oh_client.
% @end

% This chaining thing is dumb because each client is setting the
% options for the next one. Since we want the options to be "global"
% in a sense... eh it's fine as long as the client_man can propagate
% any changes (such as a dynamic update to the list of routers)
start(StartOptions) ->
    oh_client_sup:start_acceptor(StartOptions).



-spec start_link(StartOptions) -> Result
    when StartOptions :: start_options(),
         Result       :: {ok, pid()}
                       | {error, Reason},
         Reason       :: {already_started, pid()}
                       | {shutdown, term()}
                       | term().
% @private
% This is called by the oh_client_sup. While start/1 is called to iniate a startup
% (essentially requesting a new worker be started by the supervisor), this is
% actually called in the context of the supervisor.
% @end

start_link(StartOptions) ->
    proc_lib:start_link(?MODULE, init, [self(), StartOptions]).



-spec init(Parent, StartOptions) -> no_return()
    when Parent       :: pid(),
         StartOptions :: start_options().
% @private
% This is the first code executed in the context of the new worker itself.
% This function does not have any return value, as the startup return is
% passed back to the supervisor by calling proc_lib:init_ack/2.
% We see the initial form of the typical arity-3 service loop form here in the
% call to listen/3.
% @end

init(Parent, StartOptions) ->
    ok = log(info, "~p Listening.", [self()]),
    Debug = sys:debug_options([]),
    ok = proc_lib:init_ack(Parent, {ok, self()}),
    listen(Parent, Debug, StartOptions).



-spec listen(Parent, Debug, StartOptions) -> no_return()
    when Parent       :: pid(),
         Debug        :: [sys:dbg_opt()],
         StartOptions :: start_options().
% @private
% This function waits for a TCP connection. The owner of the socket
% is still the oh_client_man (so it can still close it on a call to
% oh_client_man:ignore/0), but the only one calling gen_tcp:accept/1
% on it is this process. Closing the socket is one way a manager
% process can gracefully unblock child workers that are blocking on a
% network accept.
%
% Once it makes a TCP connection it will call start/1 to spawn its
% successor.
% @end

listen(Parent, Debug, StartOptions = #{socket := ListenSocket}) ->
    case gen_tcp:accept(ListenSocket) of
        {ok, ConnectionSocket} ->
            % Spawn the next guy
            {ok, _} = start(StartOptions),
            ok = oh_client_man:enroll(),
            {ok, Peer} = inet:peername(ConnectionSocket),
            ok = tell("~p Connection accepted from: ~p", [self(), Peer]),
            State = #s{socket = ConnectionSocket},
            loop(Parent, Debug, State);
        {error, closed} ->
            ok = tell("~p Retiring: Listen socket closed.", [self()]),
            exit(normal)
     end.



-spec system_continue(Parent, Debug, State) -> no_return()
    when Parent :: pid(),
         Debug  :: [sys:dbg_opt()],
         State  :: state().
% @private
% The function called by the OTP internal functions after a system
% message has been handled. If the worker process has several
% possible states this is one place resumption of a specific state
% can be specified and dispatched.
% @end

system_continue(Parent, Debug, State) ->
    loop(Parent, Debug, State).



-spec system_terminate(Reason, Parent, Debug, State) -> no_return()
    when Reason :: term(),
         Parent :: pid(),
         Debug  :: [sys:dbg_opt()],
         State  :: state().
% @private
% Called by the OTP inner bits to allow the process to terminate
% gracefully.  Exactly when and if this is callback gets called is
% specified in the docs: See:
% http://erlang.org/doc/design_principles/spec_proc.html#msg
% @end

system_terminate(Reason, _Parent, _Debug, _State) ->
    exit(Reason).



-spec system_get_state(State) -> {ok, State}
    when State :: state().
% @private
% This function allows the runtime (or anything else) to inspect the
% running state of the worker process at any arbitrary time.
% @end

system_get_state(State) -> {ok, State}.



-spec system_replace_state(StateFun, State) -> {ok, NewState, State}
    when StateFun :: fun(),
         State    :: state(),
         NewState :: term().
% @private
% This function allows the system to update the process state
% in-place. This is most useful for state transitions between code
% types, like when performing a hot update (very cool, but sort of
% hard) or hot patching a running system (living on the edge!).
% @end

system_replace_state(StateFun, State) ->
    {ok, StateFun(State), State}.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INTERNALS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% internals

-spec loop(Parent, Debug, State) -> no_return()
    when Parent :: pid(),
         Debug  :: [sys:dbg_opt()],
         State  :: state().
% @private
% The service loop itself. This is the service state. The process
% blocks on receive of Erlang messages, TCP segments being received
% themselves as Erlang messages.
%
% This is the heart of the matter, where all the shit goes down with
% like talking to browsers and whatnot
% @end

% This is the part where parsing is happening, matches on unfinished
% parser state
%
% TODO: maybe do keepalive logic but eh isn't a major priority
loop(Parent, Debug, State = #s{socket = Socket, parser_state = PS = {partial, _, _}}) ->
    ok = inet:setopts(Socket, [{active, once}]),
    %ok = tell("parser state: ~p", [PS]),
    receive
        {tcp, Socket, Message} ->
            NewParserState = oh_parse:parse(Message, PS),
            %ok = tell("new parser state: ~p", [NewParserState]),
            NewState = State#s{parser_state = NewParserState},
            loop(Parent, Debug, NewState);
        {tcp_closed, Socket} ->
            ok = log(info, "~p Socket closed, retiring.~n", [self()]),
            exit(normal);
        {system, From, Request} ->
            sys:handle_system_msg(Request, From, Parent, ?MODULE, Debug, State);
        Unexpected ->
            ok = log(warning, "~p Unexpected message: ~tw; state: ~tw", [self(), Unexpected, State]),
            loop(Parent, Debug, State)
    % TODO: should figure out a way to dynamically adjust the timeout
    % but fuck it
    after 3000 ->
        ok = tell(info, "request timed out"),
        ResponseBytes = response_bytes_safe({timeout, PS}),
        ok = gen_tcp:send(Socket, ResponseBytes),
        % The reason I'm using zx_net:disconnect is
        %
        % - gen_tcp:shutdown was returning {error, einval} because socket
        %   was already closed
        % - was pattern matching on ok
        % - pattern match fail caused crash
        % - bunch of crashes in a row exceeded supervisor crash limit
        % - causes 502 bad gateway
        % - zx_net:disconnect handles all of the various possible socket
        %   errors, logs them, and always returns ok
        ok = zx_net:disconnect(Socket),
        ok
    end;
% This is the case where we're done parsing, not expecting any more
% tcp data (incoming tcp data go to Unexpected case)
%
% MaybeRequest means that there could have been a parse error
loop(_Parent, _Debug, _State = #s{socket = Socket, parser_state = {done, MaybeRequest}}) ->
    ok = inet:setopts(Socket, [{active, once}]),
    ok = log(info, "~p request: ~p", [self(), MaybeRequest]),
    ResponseBytes = response_bytes_safe(MaybeRequest),
    %ok = tell("~p response bytes:~n~s", [self(), ResponseBytes]),
    ok = gen_tcp:send(Socket, ResponseBytes),
    % The reason I'm using zx_net:disconnect is
    %
    % - gen_tcp:shutdown was returning {error, einval} because socket
    %   was already closed
    % - was pattern matching on ok
    % - pattern match fail caused crash
    % - bunch of crashes in a row exceeded supervisor crash limit
    % - causes 502 bad gateway
    % - zx_net:disconnect handles all of the various possible socket
    %   errors, logs them, and always returns ok
    ok = zx_net:disconnect(Socket),
    ok.



response_bytes_safe(MaybeRequest) ->
    try response_bytes(MaybeRequest) of
        {ok, Bytes} -> Bytes
    catch
        _:_ -> err_500()
    end.



err_500() ->
    Content = <<"<h1>500 Internal Server Error</h1>">>,
    Response = #rsp{content = Content, status = 500},
    oh_render:render(Response).



response_bytes(MaybeRequest) ->
    Routers = ophttpd:get_routers(),
    Handler = oh_route:route(MaybeRequest, Routers),
    % This hack is here because I'm a retard and didn't think this
    % through. See ophttpd 19, about 50 minutes in, when I realized
    % the problem that led to this
    %
    % The other option is to make programmer-defined handlers match
    % on {ok, Request}, which seems like kind of a wart. Or to do
    % some weird voodoo hack in oh_route. This is the least bad way
    % to solve my retarded lack of foreskin and planning.
    %
    % Basically due to the way I structured the hardcoded routing
    % logic
    %
    % A handler takes a request MAP (defined in oh_maps)
    % and converts it into a response MAP (also defined in oh_maps).
    %
    % The exception is the bottom handlers (bad request, request
    % timeout, and not found), which take the request data structure
    % itself, and return the response data structure. This is for
    % legacy reasons, meaning when I wrote them I hadn't had the
    % insight that it would be easier for other applications if the
    % interface was maps rather than records.
    %
    % Anyway, assuming a normal handler, we convert the
    % response_map() back into the response data structure (record),
    % and precede from there
    %
    % We then pass the response record to the
    % renderer (oh_render.erl), which turns it into bytes, and then
    % we return the bytes.
    Response =
        case MaybeRequest of
            % Valid request, do the maps dance
            {ok, ValidRequest} ->
                ValidReqMap = oh_maps:request_to_map(ValidRequest),
                ResponseMap = Handler:handle(ValidReqMap),
                Resp        = oh_maps:map_to_response(ResponseMap),
                Resp;
            % the thing passed to the router is a bad request, pass
            % the whole tuple thing to the handler (which will be
            % ohh_400_bad_request, but alas)
            BadReqTuple = {parse_error, _Reason, _PartialRequest, _BinaryQueue} ->
                % in this case, the router will have set handler to
                % ohh_400_bad_request, which returns the response
                % data structure
                Handler:handle(BadReqTuple);
            {timeout, ParserState} ->
                % in this case, the router will have set handler to
                % ohh_408_request_timeout, which returns a data
                % structure
                % Note that this handler
                Handler:handle({timeout, ParserState})
        end,
    ok = do_log(MaybeRequest, Response),
    ResponseBytes = oh_render:render(Response),
    {ok, ResponseBytes}.



do_log({ok, Request}, Response) ->
    % Log
    #req{method = Method, uri = URI} = Request,
    #rsp{status = Status} = Response,
    ReasonPhrase = oh_render:reason_phrase(Status),
    ok = tell(info,
              "\n"
              "    ~p request : Method=~s URI=\"~s\"~n"
              "    ~p response: Status=~p ReasonPhrase=\"~s\"",
              [self(), Method, URI,
               self(), Status, ReasonPhrase]),
    ok;
do_log(ParseError = {parse_error, _Reason, _PartialRequest, _BinaryQueue}, Response) ->
    ok = tell(info,
              "~n"
              "    ~p malformed request: ~w~n"
              "    ~p response         : ~w",
              [self(), ParseError,
               self(), Response]),
    ok;
do_log({timeout, ParserState}, Response) ->
    ok = tell(info,
              "~n"
              "    ~p timeout~n"
              "    ~p parser state     : ~w~n"
              "    ~p response         : ~w",
              [self(),
               self(), ParserState,
               self(), Response]),
    ok.
