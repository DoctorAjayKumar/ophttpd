% @doc
% This is the "client manager", which is the thing that manages the
% things that talk to web browsers etc (which are called "client"s)
%
% All of the "global state" needs to be handled here
%
% This is the meat and potatoes of the HTTP server, this is the thing
% that handles all the other things
% @end
-module(oh_client_man).
-vsn("0.1.0").
-behavior(gen_server).

-export([listen/1, ignore/0, get_port_num/0]).
-export([get_routers/0, set_routers/1]).
-export([enroll/0, echo/1]).
-export([start_link/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         code_change/3, terminate/2]).


%%% Type and Record Definitions
-record(s, {port_num = none :: none | inet:port_number(),
            listener = none :: none | gen_tcp:socket(),
            clients  = []   :: [pid()],
            routers  = []   :: [atom()]}).

-type state()   :: #s{}.

-include("$zx_include/zx_logger.hrl").



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% API: EXPORTED FUNCTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Service Interface

-spec listen(PortNum) -> Result
    when PortNum :: inet:port_number(),
         Result  :: ok
                  | {error, Reason},
         Reason  :: {listening, inet:port_number()}.
% @doc
% Tell the service to start listening on a given port.
% Only one port can be listened on at a time in the current implementation, so
% an error is returned if the service is already listening.
% @end

listen(PortNum) ->
    gen_server:call(?MODULE, {listen, PortNum}).



-spec ignore() -> ok.
% @doc
% Tell the service to stop listening.
% It is not an error to call this function when the service is not listening.
% @end

ignore() ->
    gen_server:cast(?MODULE, ignore).



-spec get_port_num() -> {listening, inet:port_number()} | not_listening.
% @doc
% get the current port number
% @end

get_port_num() ->
    gen_server:call(?MODULE, get_port_num).



-spec get_routers() -> [atom()].
% @doc
% get the current list of routers
% @end

get_routers() ->
    gen_server:call(?MODULE, get_routers).



-spec set_routers([atom()]) -> ok.
% @doc
% set the current list of routers
% @end

set_routers(Routers) ->
    gen_server:cast(?MODULE, {set_routers, Routers}).



%%% Client Process Interface

-spec enroll() -> ok.

% @doc
% Clients register here when they establish a connection.
% Other processes can enroll as well.
% @end

enroll() ->
    ok = io:format("~p ~p", [?MODULE, ?LINE]),
    gen_server:cast(?MODULE, {enroll, self()}).



-spec echo(Message) -> ok
    when Message :: string().
% @doc
% The function that tells the manager to broadcast a message to all clients.
% This can broadcast arbitrary strings to clients from non-clients as well.
% @end

echo(Message) ->
    gen_server:cast(?MODULE, {echo, Message, self()}).



%%% Startup Functions

-spec start_link() -> Result
    when Result :: {ok, pid()}
                 | {error, Reason :: term()}.
% @private
% This should only ever be called by oh_clients (the service-level
% supervisor).
% @end

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, none, []).



-spec init(none) -> {ok, state()}.
% @private
% Called by the supervisor process to give the process a chance to perform any
% preparatory work necessary for proper function.
% @end

init(none) ->
    ok = io:format("Starting.~n"),
    State = #s{},
    {ok, State}.



%%% gen_server Message Handling Callbacks

-spec handle_call(Message, From, State) -> Result
    when Message  :: term(),
         From     :: {pid(), reference()},
         State    :: state(),
         Result   :: {reply, Response, NewState}
                   | {noreply, State},
         Response :: ok
                   | {error, {listening, inet:port_number()}},
         NewState :: state().
% @private
% The gen_server:handle_call/3 callback.
% See: http://erlang.org/doc/man/gen_server.html#Module:handle_call-3
% @end

handle_call({listen, PortNum}, _, State) ->
    {Response, NewState} = do_listen(PortNum, State),
    {reply, Response, NewState};
handle_call(get_port_num, _From, State) ->
    Response = do_get_port_num(State),
    {reply, Response, State};
handle_call(get_routers, _From, State = #s{routers = Routers}) ->
    {reply, Routers, State};
handle_call(Unexpected, From, State) ->
    ok = io:format("~p Unexpected call from ~tp: ~tp~n", [self(), From, Unexpected]),
    {noreply, State}.



-spec handle_cast(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().
% @private
% The gen_server:handle_cast/2 callback.
% See: http://erlang.org/doc/man/gen_server.html#Module:handle_cast-2
% @end

handle_cast({enroll, Pid}, State) ->
    ok = io:format("~p ~p", [?MODULE, ?LINE]),
    NewState = do_enroll(Pid, State),
    {noreply, NewState};
handle_cast({echo, Message, Sender}, State) ->
    ok = do_echo(Message, Sender, State),
    {noreply, State};
handle_cast(ignore, State) ->
    NewState = do_ignore(State),
    {noreply, NewState};
handle_cast({set_routers, Routers}, State) ->
    NewState = do_set_routers(Routers, State),
    {noreply, NewState};
handle_cast(Unexpected, State) ->
    ok = log(warning, "~p Unexpected cast: ~tp~n", [self(), Unexpected]),
    {noreply, State}.



-spec handle_info(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().
% @private
% The gen_server:handle_info/2 callback.
% See: http://erlang.org/doc/man/gen_server.html#Module:handle_info-2
% @end
handle_info({'DOWN', Mon, process, Pid, Reason}, State) ->
    NewState = handle_down(Mon, Pid, Reason, State),
    {noreply, NewState};
handle_info(Unexpected, State) ->
    ok = io:format("~p Unexpected info: ~tp~n", [self(), Unexpected]),
    {noreply, State}.



%%% OTP Service Functions

-spec code_change(OldVersion, State, Extra) -> Result
    when OldVersion :: {down, Version} | Version,
         Version    :: term(),
         State      :: state(),
         Extra      :: term(),
         Result     :: {ok, NewState}
                     | {error, Reason :: term()},
         NewState   :: state().
% @private
% The gen_server:code_change/3 callback.
% See: http://erlang.org/doc/man/gen_server.html#Module:code_change-3
% @end

code_change(_, State, _) ->
    {ok, State}.



-spec terminate(Reason, State) -> no_return()
    when Reason :: normal
                 | shutdown
                 | {shutdown, term()}
                 | term(),
         State  :: state().
% @private
% The gen_server:terminate/2 callback.
% See: http://erlang.org/doc/man/gen_server.html#Module:terminate-2
% @end
terminate(_, _) ->
    ok.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INTERNALS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Doer Functions

-spec do_listen(PortNum, State) -> {Result, NewState}
    when PortNum  :: inet:port_number(),
         State    :: state(),
         Result   :: ok
                   | {error, Reason :: {listening, inet:port_number()}},
         NewState :: state().

do_listen(PortNum, State = #s{port_num = none}) ->
    SocketOptions =
        [inet6,
         {packet,    0},
         {active,    once},
         {mode,      binary},
         {keepalive, true},
         {reuseaddr, true}],
    {ok, Listener} = gen_tcp:listen(PortNum, SocketOptions),
    % This clunky thing where this type of logic is spread over
    % multiple files needs to be cleaner, but for now this gets tits
    % on the screen.
    StartOptions = #{socket => Listener},
    {ok, _} = oh_client:start(StartOptions),
    {ok, State#s{port_num = PortNum, listener = Listener}};
do_listen(_, State = #s{port_num = PortNum}) ->
    ok = tell(error, "~p Already listening on ~p", [self(), PortNum]),
    {{error, {listening, PortNum}}, State}.



-spec do_ignore(State) -> NewState
    when State    :: state(),
         NewState :: state().

do_ignore(State = #s{listener = none}) ->
    State;
do_ignore(State = #s{listener = Listener}) ->
    ok = gen_tcp:close(Listener),
    State#s{port_num = none, listener = none}.



-spec do_get_port_num(State) -> Result
    when State  :: state(),
         Result :: {listening, inet:port_number()}
                 | not_listening.
% @doc
% get the current port number
% @end

do_get_port_num(#s{port_num = none}) ->
    not_listening;
do_get_port_num(#s{port_num = PortNum}) ->
    {listening, PortNum}.



-spec do_enroll(Pid, State) -> NewState
    when Pid      :: pid(),
         State    :: state(),
         NewState :: state().

do_enroll(Pid, State = #s{clients = Clients}) ->
    ok = io:format("~p ~p", [?MODULE, ?LINE]),
    case lists:member(Pid, Clients) of
        false ->
            Mon = monitor(process, Pid),
            ok = tell("Monitoring ~tp @ ~tp~n", [Pid, Mon]),
            State#s{clients = [Pid | Clients]};
        true ->
            State
    end.



-spec do_echo(Message, Sender, State) -> ok
    when Message :: string(),
         Sender  :: pid(),
         State   :: state().

do_echo(Message, Sender, #s{clients = Clients}) ->
    Send = fun(Client) -> Client ! {relay, Sender, Message} end,
    lists:foreach(Send, Clients).



-spec do_set_routers(Routers, State) -> NewState
    when Routers  :: string(),
         State    :: state(),
         NewState :: state().

do_set_routers(Routers, State = #s{clients = Clients}) ->
    ok = tell(info, "client manager setting new routers: ~p", [Routers]),
    ok = tell(info, "clients: ~p", [Clients]),
    % Send =
    %     fun(Client) ->
    %         ok = tell(info, "telling client ~p to set routers", [Client]),
    %         Client ! {set_routers, Routers}
    %     end,
    % ok = lists:foreach(Send, Clients),
    NewState = State#s{routers = Routers},
    NewState.



-spec handle_down(Mon, Pid, Reason, State) -> NewState
    when Mon      :: reference(),
         Pid      :: pid(),
         Reason   :: term(),
         State    :: state(),
         NewState :: state().
% @private
% Deal with monitors. When a new process enrolls as a client a
% monitor is set and the process is added to the client list. When
% the process terminates we receive a 'DOWN' message from the
% monitor. More sophisticated work managers typically have an
% "unenroll" function, but this echo service doesn't need one.
% @end

handle_down(Mon, Pid, Reason, State = #s{clients = Clients}) ->
    Unexpected = {'DOWN', Mon, process, Pid, Reason},
    tell("~p down: ~p", [self(), Unexpected]),
    case lists:member(Pid, Clients) of
        true ->
            NewClients = lists:delete(Pid, Clients),
            State#s{clients = NewClients};
        false ->
            State
    end.
