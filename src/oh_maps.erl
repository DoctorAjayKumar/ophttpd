% @doc
% pure code module for converting between the request/response data
% structures and maps
%
% note the "response map partial" thing is to avoid dialyzer errors.
% any response map gets merged against a default response map. there
% are default values to each field (status is assumed to be 200 for
% instance). You don't need to specify all of them.
-module(oh_maps).

-export([
    request_to_map/1,
    map_to_response/1
]).

-export_type([
    headers/0,
    request_map/0,
    response_map/0,
    response_map_partial/0
]).

-include("oh.hrl").

-type request_map() :: #{method   := 'GET' | 'POST',
                         uri      := string(),
                         version  := http11,
                         options  := headers(),
                         body_len := integer(),
                         body     := binary(),
                         full_req := binary()}.

-type response_map_partial() :: map().

-type response_map() :: #{status        := integer(),
                          content_type  := string(),
                          last_modified := calendar:datetime(),
                          connection    := closed | keep_alive,
                          content       := binary()}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% API
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-spec request_to_map(request()) -> request_map().

request_to_map(#req{method   = M,
                    uri      = U,
                    version  = V,
                    options  = O,
                    body_len = BL,
                    body     = B,
                    full_req = R}) ->
    #{method   => M,
      uri      => U,
      version  => V,
      options  => O,
      body_len => BL,
      body     => B,
      full_req => R}.



-spec map_to_response(response_map_partial()) -> response().

map_to_response(RM) ->
    RM2 = maps:merge(response_map_default(), RM),
    really_map_to_response(RM2).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% INTERNALS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

response_map_default() ->
        #{status        => 200,
          content_type  => "text/html",
          last_modified => calendar:universal_time(),
          connection    => closed,
          content       => <<>>}.

really_map_to_response(#{status        := R,
                         content_type  := CT,
                         last_modified := LM,
                         connection    := Conn,
                         content       := Cont}) ->
    #rsp{status        = R,
         content_type  = CT,
         last_modified = LM,
         connection    = Conn,
         content       = Cont}.
