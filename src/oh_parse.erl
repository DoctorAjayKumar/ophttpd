-module(oh_parse).

-export_type([
    state/0
]).
-export([
    new/0,
    parse/2
]).

-include("oh.hrl").


-type queue()   :: binary().
-type state()   :: {partial, request(), queue()}
                 | {done, {ok, request()}}
                 | {done, {parse_error, Reason :: any(), request(), queue()}}.

%%% API

-spec new() -> state().
% @doc
% Blank parser state
% @end

new() ->
    {partial, #req{}, <<>>}.



-spec parse(binary(), state()) -> state().
% @doc
% Parse new binary string
% @end

parse(Binary, {partial, Req, Q}) ->
    #req{full_req = OldFullReq} = Req,
    NewFullReq = <<OldFullReq/binary, Binary/binary>>,
    NewReq = Req#req{full_req = NewFullReq},
    NewQ = <<Q/binary, Binary/binary>>,
    parse({partial, NewReq, NewQ}).



%%% internals

-spec parse(state()) -> state().
% @private
% internal parsing thing
% @end

% first parse the method
% can definitely read get
parse({partial, Req = #req{method = none}, <<"GET ", Rest/binary>>}) ->
    ok = zx:tell("hello"),
    NewReq = Req#req{method = 'GET'},
    NewState = {partial, NewReq, Rest},
    parse(NewState);
% can definitely read post
parse({partial, Req = #req{method = none}, <<"POST ", Rest/binary>>}) ->
    NewReq = Req#req{method = 'POST'},
    NewState = {partial, NewReq, Rest},
    parse(NewState);
% not enough characters to decide
parse(Result = {partial, _Req = #req{method = none}, Queue}) when byte_size(Queue) < 5 ->
    Result;
% enough characters to decide, error
parse({partial, Req = #req{method = none}, Queue}) when byte_size(Queue) >= 5 ->
    {done, {parse_error, invalid_method, Req, Queue}};
% next parse the uri; note the prior thing consumes a space, so we're
% just consuming until we hit a space
parse({partial, Req = #req{uri = {uri_partial, PartialURI}}, <<" ", Rest/binary>>}) ->
    NewReq = Req#req{uri = lists:reverse(PartialURI)},
    NewQ = Rest,
    parse({partial, NewReq, NewQ});
% not a space
parse({partial, Req = #req{uri = {uri_partial, PartialURI}}, <<NotSpace, Rest/binary>>}) ->
    NewReq = Req#req{uri = {uri_partial, [NotSpace | PartialURI] }},
    NewQ = Rest,
    parse({partial, NewReq, NewQ});
% out of characters
parse(Result = {partial, _Req = #req{uri = {uri_partial, _PartialURI}}, <<>>}) ->
    Result;
% next parse the version; wait until we get the full thing.
parse({partial, Req = #req{version = none}, <<"HTTP/1.1\r\n", Rest/binary>>}) ->
    NewReq = Req#req{version = http11},
    NewState = {partial, NewReq, Rest},
    parse(NewState);
% not enough characters to decide
parse(Result = {partial, _Req = #req{version = none}, Queue}) when byte_size(Queue) < 10 ->
    Result;
% enough characters to decide, error
parse({partial, Req = #req{version = none}, Queue}) when byte_size(Queue) >= 10 ->
    {done, {parse_error, invalid_version, Req, Queue}};
% parse headers
% we know we are at the end of the headers when we hit two newlines
% in a row
% two newline, done parsing headers
parse({partial, Req = #req{options = {hdrs_partial, HdrsRawRevsd}}, <<"\r\n\r\n", Rest/binary>>}) ->
    HdrsRaw = lists:reverse(HdrsRawRevsd),
    {Headers, BodyLen} = split_headers(HdrsRaw),
    NewReq = Req#req{options = Headers,
                     body_len = BodyLen},
    parse({partial, NewReq, Rest});
% If we have less than 5 characters in the queue, wait for more
% characters, because for instance we don't want to match on "\r\n\r"
% At this point, we're guaranteed to there being at least 5 more
% characters in a valid request
parse(Result = {partial, _Req = #req{options = {hdrs_partial, _}}, Queue}) when byte_size(Queue) < 5 ->
    Result;
% If we have at least 5 characters in the queue, grab the first
% character and add him to the headers
parse({partial, Req = #req{options = {hdrs_partial, HdrsRawRevsd}}, Queue}) when byte_size(Queue) >= 5 ->
    <<C, Rest/binary>> = Queue,
    NewHdrsRawRevsd = [C | HdrsRawRevsd],
    NewReq = Req#req{options = {hdrs_partial, NewHdrsRawRevsd}},
    NewQueue = Rest,
    parse({partial, NewReq, NewQueue});
% parse the body
% Not enouch characters, wait for more
parse(Result = {partial, _Req = #req{body_len = BL, body = none}, Queue}) when byte_size(Queue) < BL ->
    Result;
% enough characters, grab them
parse({partial, Req = #req{body_len = BL, body = none}, Queue}) when byte_size(Queue) =:= BL ->
    FinalReq = Req#req{body = Queue},
    % we're done!
    {done, {ok, FinalReq}};
% too many characters! error
parse({partial, Req = #req{body_len = BL, body = none}, Queue}) when byte_size(Queue) > BL ->
    % we're done!
    {done, {parse_error, body_too_long, Req, Queue}};
% should never reach this case, but pc_load_letter just in case
parse({partial, Req, Queue}) ->
    {done, {parse_error, pc_load_letter, Req, Queue}}.



-spec split_headers(string()) -> {Headers :: [{string(), string()}],
                                  BodyLen :: integer()}.
% @private
% split the headers
% @end

split_headers(Str) ->
    Lines = string:split(Str, "\r\n", all),
    {Pairs, BodyLen} = pairs(Lines, [], 0),
    {Pairs, BodyLen}.



pairs([], Pairs, BodyLen) ->
    {Pairs, BodyLen};
pairs([Line | Lines], Pairs, 0) ->
    [K, V] = string:split(Line, ": "),
    CL =
        case K of
            "Content-Length" -> erlang:list_to_integer(V);
            _                -> 0
        end,
    pairs(Lines, [{K, V} | Pairs], CL);
pairs([Line | Lines], Pairs, CL) ->
    [K, V] = string:split(Line, ": "),
    pairs(Lines, [{K, V} | Pairs], CL).
