-module(oh_render).

-export([
    render/1,
    reason_phrase/1
]).

-include("oh.hrl").

render(#rsp{status        = Status,
            content_type  = CT,
            last_modified = LM,
            content       = Content}) ->
    % this is to prevent that dumbass image content length bug
    true = is_binary(Content),
    [status_line(Status),
     date_line(),
     server_line(),
     last_modified_line(LM),
     content_type_line(CT),
     content_len_line(Content),
     blank_line(),
     Content].

status_line(StatusN) ->
    io_lib:format("HTTP/1.1 ~p ~s\r\n", [StatusN, reason_phrase(StatusN)]).

date_line() ->
    ["Date: ", oh_date:right_now(), "\r\n"].

server_line() ->
    ["Server: ophttpd ", ophttpd:version(), "\r\n"].

last_modified_line(LM) ->
    ["Last-Modified: ", oh_date:format(LM), "\r\n"].

content_type_line(CT) ->
    ["Content-Type: ", CT, "\r\n"].

content_len_line(Content) ->
    ContentLen = erlang:byte_size(Content),
    io_lib:format("Content-Length: ~p\r\n", [ContentLen]).

blank_line() ->
    "\r\n".


% copied from https://www.freesoft.org/CIE/RFC/2068/43.htm
reason_phrase(100) -> "Continue";
reason_phrase(101) -> "Switching Protocols";
reason_phrase(200) -> "OK";
reason_phrase(201) -> "Created";
reason_phrase(202) -> "Accepted";
reason_phrase(203) -> "Non-Authoritative Information";
reason_phrase(204) -> "No Content";
reason_phrase(205) -> "Reset Content";
reason_phrase(206) -> "Partial Content";
reason_phrase(300) -> "Multiple Choices";
reason_phrase(301) -> "Moved Permanently";
reason_phrase(302) -> "Moved Temporarily";
reason_phrase(303) -> "See Other";
reason_phrase(304) -> "Not Modified";
reason_phrase(305) -> "Use Proxy";
reason_phrase(400) -> "Bad Request";
reason_phrase(401) -> "Unauthorized";
reason_phrase(402) -> "Payment Required";
reason_phrase(403) -> "Forbidden";
reason_phrase(404) -> "Not Found";
reason_phrase(405) -> "Method Not Allowed";
reason_phrase(406) -> "Not Acceptable";
reason_phrase(407) -> "Proxy Authentication Required";
reason_phrase(408) -> "Request Time-out";
reason_phrase(409) -> "Conflict";
reason_phrase(410) -> "Gone";
reason_phrase(411) -> "Length Required";
reason_phrase(412) -> "Precondition Failed";
reason_phrase(413) -> "Request Entity Too Large";
reason_phrase(414) -> "Request-URI Too Large";
reason_phrase(415) -> "Unsupported Media Type";
reason_phrase(500) -> "Internal Server Error";
reason_phrase(501) -> "Not Implemented";
reason_phrase(502) -> "Bad Gateway";
reason_phrase(503) -> "Service Unavailable";
reason_phrase(504) -> "Gateway Time-out";
reason_phrase(505) -> "HTTP Version not supported".
