-module(ohh_404_not_found).

-export([
    handle/1
]).

-include("oh.hrl").



-spec handle(any()) -> ophttpd:response_map_partial().
% @doc
% this returns a response MAP, because from the oh_client perspective
% this is a valid request
%
% some shit like that. idk

handle(X) ->
    zx:log(info, "~p/~p/~p 404 Not Found: ~tw", [self(), ?MODULE, ?LINE, X]),
    #{status  => 404,
      content => content()}.


content() ->
    unicode:characters_to_binary([
        "<!DOCTYPE html>"
        "<html>"
            "<head>"
                "<title>"
                    "404 Not Found | ophttpd ", ophttpd:version(),
                "</title>"
            "</head>"
            "<body>"
                "<h1>404 Not Found</h1>"
                "<p>"
                    "ophttpd ", ophttpd:version(),
                "</p>"
            "</body>"
        "</html>"
    ]).
