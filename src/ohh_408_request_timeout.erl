-module(ohh_408_request_timeout).

-export([
    handle/1
]).

-include("oh.hrl").


-spec handle(any()) -> response().
% @doc
% this returns the response DATA STRUCTURE because legacy reasons. I
% had not had the "other applications want to talk using maps"
% insight before writing this, and i'm too lazy to change everything
% over

handle(X) ->
    zx:tell("~p 408 Request Timeout: ~tw", [self(), X]),
    #rsp{status = 408,
         content = content()}.


content() ->
    unicode:characters_to_binary([
        "<!DOCTYPE html>"
        "<html>"
            "<head>"
                "<title>"
                    "408 Request Timeout | ophttpd ", ophttpd:version(),
                "</title>"
            "</head>"
            "<body>"
                "<h1>408 Request Timeout</h1>"
                "<p>"
                    "ophttpd ", ophttpd:version(),
                "</p>"
            "</body>"
        "</html>"
    ]).
