% @doc
% The built-in hardcoded routing logic
% @end
-module(oh_route).

-export([
    route/2
]).

-include("oh.hrl").

-type router() :: atom().
-type handler() :: atom().
-type maybe_request() :: {ok, request()}
                       | {parse_error, Reason :: any(), any(), any()}
                       | {timeout, PartialParserState :: any()}.

%%% API

-spec route(MaybeReq, Routers) -> Handler
    when MaybeReq :: maybe_request(),
         Routers  :: [router()],
         Handler  :: handler().
%%% @doc
%%% Determine a the handler to handle a given request
%%% @end

route({parse_error, _, _, _}, _) -> ohh_400_bad_request;
route({timeout, _}, _)           -> ohh_408_request_timeout;
route({ok, Req}, Routers)        -> route_ok(Req, Routers).

% fold over the list of routers until one of them says he can handle
% the request
%
% If all of them say no, default to ohh_404_not_found
route_ok(Req = #req{method = Method, uri = URI}, [RouterModule | Rest]) ->
    CanTheRouterHandleThisRequest = RouterModule:can_you_handle_this(Method, URI),
    % If he can handle this, return his name
    % If he can't, try with the rest
    case CanTheRouterHandleThisRequest of
        {yes, Handler} ->
            Handler;
        no ->
            route_ok(Req, Rest)
    end;
% out of routers, return 404
route_ok(_Req, []) ->
    ohh_404_not_found.
