% @doc
% Formatter for HTTP's retarded date format.
% @end
-module(oh_date).

-export([right_now/0, format/1]).

right_now() ->
    format(calendar:universal_time()).

format({YMD, _HMS} = {{Y, Mon, D}, {H, Min, S}}) ->
    DOW     = calendar:day_of_the_week(YMD),
    DOW_str = dow_str(DOW),
    DOM_str = dom_str(D),
    Mon_str = mon_str(Mon),
    Y_str   = yr_str(Y),
    H_str   = hr_str(H),
    Min_str = min_str(Min),
    S_str   = sec_str(S),
    [DOW_str, ", ", DOM_str, " ", Mon_str, " ", Y_str, " ",
     H_str, ":", Min_str, ":", S_str, " GMT"].


dom_str(DOM) -> io_lib:format("~2..0w", [DOM]).
yr_str(Yr)   -> io_lib:format("~4...w", [Yr]).
hr_str(Hr)   -> io_lib:format("~2..0w", [Hr]).
min_str(Min) -> io_lib:format("~2..0w", [Min]).
sec_str(Sec) -> io_lib:format("~2..0w", [Sec]).


mon_str( 1) -> "Jan";
mon_str( 2) -> "Feb";
mon_str( 3) -> "Mar";
mon_str( 4) -> "Apr";
mon_str( 5) -> "May";
mon_str( 6) -> "Jun";
mon_str( 7) -> "Jul";
mon_str( 8) -> "Aug";
mon_str( 9) -> "Sep";
mon_str(10) -> "Oct";
mon_str(11) -> "Nov";
mon_str(12) -> "Dec".


dow_str(1) -> "Sun";
dow_str(2) -> "Mon";
dow_str(3) -> "Tue";
dow_str(4) -> "Wed";
dow_str(5) -> "Thu";
dow_str(6) -> "Fri";
dow_str(7) -> "Sat".
