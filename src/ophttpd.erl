-module(ophttpd).
-vsn("0.1.0").
-behavior(application).

% getters and setters
% startup and shutdown
-export([
    start/0,
    start/1
]).

% core functionality (things you call a lot)
-export([
    listen/1,
    ignore/0,
    set_routers/1
]).

% miscellany (things that exist for good reason but are probably
% never called)
-export([
    version/0,
    get_port_num/0,
    get_routers/0
]).

% otp callbacks
-export([
    start/2,
    stop/1
]).

% types
-export_type([
    request_map/0,
    response_map/0,
    response_map_partial/0
]).

-include("$zx_include/zx_logger.hrl").

-type request_map() :: oh_maps:request_map().
-type response_map() :: oh_maps:response_map().
-type response_map_partial() :: oh_maps:response_map_partial().

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% API: startup and shutdown
%
% - start/0: just start in null state
% - start/1: start and listen to the given port
%
% start/* callgraph
%
%   start/1 ->
%       start/0
%       listen/1
%
%   start/0 ->
%       application:start/1
%
%   application:start/1 ->
%       start/2
%
%   start/2 ->
%       oh_sup:start_link/0
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-spec start() -> ok.
% @doc
% Start the server in an "ignore" state.
% @end

start() ->
    ok = application:ensure_started(sasl),
    ok = application:start(ophttpd),
    io:format("Starting...").



-spec start(PortNum) -> ok
    when PortNum :: inet:port_number().
% @doc
% Start the server and begin listening immediately. Slightly more
% convenient when playing around in the shell.
% @end

start(PortNum) ->
    ok = start(),
    ok = listen(PortNum),
    ok.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% API: functions the user is likely to need to call
%
% - listen/1: listen to a port
% - ignore/0: stop listening
% - set_routers/1: set the routers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-spec listen(PortNum) -> Result
    when PortNum :: inet:port_num(),
         Result  :: ok | {error, {listening, inet:port_num()}}.
% @doc
% Make the server start listening on a port.
% Returns an {error, Reason} tuple if it is already listening.

listen(PortNum) ->
    ok = oh_client_man:listen(PortNum),
    ok = tell("listening on ~w", [PortNum]),
    ok.



-spec ignore() -> ok.
% @doc
% Make the server stop listening if it is, or continue to do nothing
% if it isn't.

ignore() ->
    oh_client_man:ignore().



-spec set_routers(Routers :: [atom()]) -> ok.
% @doc
% Set the current list of routers (do NOT include ohh_404 or
% whatever it's called, which is the "bottom router")

set_routers(Routers) ->
    oh_client_man:set_routers(Routers).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% API: miscellany
%
% - version/0: get the ophttpd version
% - get_port_num/0: get the current port number if listening
% - get_routers/0: get the current list of routers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-spec version() -> string().
% @doc
% Version of ophttpd

version() ->
    proplists:get_value(vsn, ophttpd:module_info(attributes)).



-spec get_port_num() -> {listening, inet:port_number()} | not_listening.
% @doc
% get the current port number if listening

get_port_num() ->
    oh_client_man:get_port_num().



-spec get_routers() -> [atom()].
% @doc
% Get the current list of routers (does NOT include ohh_404 or
% whatever it's called, which is the "bottom router")

get_routers() ->
    oh_client_man:get_routers().



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% API: OTP callbacks
%
% - start/2
% - stop/2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-spec start(normal, term()) -> {ok, pid()}.
% @doc
% Called by OTP to kick things off. This is for the use of the
% "application" part of OTP, not to be called by user code.
% See: http://erlang.org/doc/apps/kernel/application.html
%
% This is the start function that gets called on zx runlocal

start(normal, _Args) ->
    % crashes if start fails
    Result = {ok, _pid} = oh_sup:start_link(),
    %ok = observer:start(),
    % port() used to be just `port() -> 8080.`
    %ok = listen(port()),
    Result.



-spec stop(term()) -> ok.
% @doc
% Similar to start/2 above, this is to be called by the "application"
% part of OTP, not client code. Causes a (hopefully graceful)
% shutdown of the application.

stop(_State) ->
    ok.
