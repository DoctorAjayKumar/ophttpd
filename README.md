# `ophttpd` = orange pill http daemon

This is a very simple zompable web server written in Erlang, for
Erlang applications. It works as follows:

- you start it up and it does nothing
- if you want ophttpd to listen to a port, call
  `ophttpd:listen(PortNum)`
- There are two things you need to know about: **{router}** s and
  **{handler}** s.

- A **{router}** is simply a module (that you write) that exports a
  function called `can_you_handle_this` with the following typespec

  ```erlang
  -spec can_you_handle_this(RequestMethod, RequestRoute) -> Result
      when RequestMethod :: ophttpd:request_method(),     % 'GET', 'POST', etc
           RequestRoute  :: binary(),
           Result        :: {yes, Handler}
                          | no,
           Handler       :: atom().
  ```

- A **{handler}** is a module (that you write) that exports a
  function called `handle` with the following typespec

  ```erlang
  -spec handle(Request) -> Response
      when Request  :: ophttpd:request(),
           Response :: ophttpd:response().
  ```

- You give ophttpd a list of routers. It goes through them in order,
  asks each of them if it can handle the request, until one of them
  says yes. If all of them say no, ophttpd forwards the request to
  `ohh_404_not_found`.

- ophttpd has only 3 built-in handlers, and they are hardcoded in:

  - a `400 Bad Request` handler for malformed requests
  - a `404 Not Found` handler if all of your routers say `no`
  - a `408 Request Timeout` handler in the event of a timeout

