# todo

- switch records to maps for external interface

# old

- get local zomp realm going
- put my website up in its own project
- have a separate project which is for serving static files with
  ophttpd
- edit ophttpd such that the README is correct
- have command line option to start and listen to a specific port
  number
- that will use `zx_daemon:argv` or whatever it is
- need to expose a function to add routers, inspect current routers
- need a more sophisticated start function with a fancier list of
  options

- eventually I will want each client to be able to serve multiple
  requests (because that's a thing HTTP/1.1 can do)

- that will require the client to be able to dynamically update the
  routers list

- `update_routers` will have to be a cast in `oh_client_man`
- and also `get_routers`
- not hard. it's like the relay logic in the chat server
- more aggressive dialyzing would catch a lot of errors
- is probably better if an application developer is told they can
  only have one router, and they have to implement the multilayered
  routing logic themselves, and that would probably eliminate a lot
  of potential bugs. Will think about and ponder. Need a break

# Getters and setters

- DONE `ophttpd:listen/1` tells ophttpd to listen to a specified port
- DONE `ophttpd:ignore/0` tells ophttpd to stop listening
- DONE `ophttpd:get_port_num/0` gets the current port number
- DONE `ophttpd:get_routers/0` gets the current list of routers

- ackshully should just have the clients call `ophttpd:get_routers()`
  whenever they need the routing info.

  use ets if I need lolspeed O(1) concurrent reads

- need to rewrite so clients don't store the router in their state

- `ophttpd:set_routers/0` sets the current list of routers


- start with port number and routers options
